# Ravata Solutions
# Inventory Management System
# October 11, 2018
# Written by Neil Arakkal

from openpyxl import load_workbook
from openpyxl.styles import Font
import os
import time
from pprint import pprint
from datetime import datetime

inv = {} #stores all the inventory items as a dictionary with its barcode as a key
updates = [] #tracks all updates made before saving changes

# MACROS FOR EACH COLUMN IN SPREADSHEET
barColumn = 'A'
manufacturerColumn = 'B'
shorthandColumn = 'C'
nameColumn = 'D'
unitsColumn = 'E'
stationColumn = 'F'
paColumn = 'G'

#decides the menu delay for some menu functions in seconds
menuDelay = 1.5

#Update() class - stores information on each update
class Update():
    type = ''
    barcode = ''
    row = ''
    shorthand = ''
    name = ''
    units = ''
    station = ''
    delta = ''

    #Initializer used to add value to update parameters
    def __init__(self, type, barcode, row, shorthand, name, units, delta, station):
        self.type = type
        self.barcode = barcode
        self.row = row
        self.shorthand = shorthand
        self.name = name
        self.units = units
        self.delta = delta
        self.station = station


    #return string when object is printed
    def __str__(self):
        return "Update:\nType = " + self.type + '\nBarcode: ' + self.barcode +  '\nRow = ' + str(self.row) + '\nShorthand = ' + self.shorthand + '\nName = ' + self.name + '\nUnits = ' + str(self.units) + '\nDelta = ' + str(self.delta) + '\nStation = ' + self.station + '\n'

    #return string when object is printed
    def __repr__(self):
        return "Update:\nType = " + self.type + '\nBarcode: ' + self.barcode +  '\nRow = ' + str(self.row) + '\nShorthand = ' + self.shorthand + '\nName = ' + self.name + '\nUnits = ' + str(self.units) + '\nDelta = ' + str(self.delta) + '\nStation = ' + self.station + '\n'


#load the database into memory, throws an exception if not present
try:
    wb = load_workbook('./inv.xlsx')
    sheet = wb['Live with Barcode']
except:
    print "Database not found, please make sure that it is in the same directory as the python file and is named 'inv.xlsx'."
    quit()

#importDatabase() - import database from spreadsheet to memory
def importDatabase():

    #Throw an error if the spreadsheet is not in the correct format for the application to read
    if not sheet[barColumn + '1'].value == 'Code':
        throwError('A')
    if not sheet[manufacturerColumn + '1'].value == 'Manufacturer':
        throwError('B')
    if not sheet[shorthandColumn + '1'].value == 'Shorthand Name':
        throwError('C')
    if not sheet[nameColumn + '1'].value == 'Item Name':
        throwError('D')
    if not sheet[unitsColumn + '1'].value == 'Units':
        throwError('E')
    if not sheet[stationColumn + '1'].value == 'Station':
        throwError('F')
    if not sheet[paColumn + '1'].value == 'PA0027':
        throwError('G')

    for currRow in range (2, sheet.max_row): #loop goes through all relevant rows
        barcode = sheet[barColumn + str(currRow)].value # extracts barcode of item

        #creates an information dictionary for the item
        details = {}
        details['row'] = currRow
        details['manufacturer'] = sheet[manufacturerColumn + str(currRow)].value
        details['shorthand'] = sheet[shorthandColumn + str(currRow)].value
        details['name'] = sheet[nameColumn + str(currRow)].value
        details['units'] = str(sheet[unitsColumn + str(currRow)].value)
        details['station'] = sheet[stationColumn + str(currRow)].value

        inv[barcode] = details #stores the information dictionary inside the main inventory dictionary with the barcode as the key

def throwError(col):
    print 'Spreadhseet not in the correct format. Please make sure that it is formatted with the following columns with no trailing whitespaces:'
    print 'Code | Manufacturer | Shorthand Name | Item Name | Units | Station | PA0027'
    print "Error detected in column '" + col + "'"
    quit()

#printInfoFromBarcode() - print item information from barcode
def printInfoFromBarcode(barcode):
    try:
        print 'Manufacturer: ' + inv[barcode]['manufacturer']
    except:
        print "Manufacturer: None found in database"
    print 'Shorthand Name: ' + inv[barcode]['shorthand']
    print 'Name: ' + inv[barcode]['name']
    print 'Units: ' + inv[barcode]['units']
    print 'Station: '+ inv[barcode]['station']
    print 'Row in Database: ' + str(inv[barcode]['row'])
    print '\n'

#menuHeader() - display menu header
def menuHeader():
    os.system('clear')
    print '-----------------------------'
    print 'Inventory Management Software'
    print '-----------------------------\n'

# mainMenu() - print main menu and accept user response
def mainMenu():
    while True:
        menuHeader()
        print '1. View Item Information'
        print '2. Add New Inventory Item'
        print '3. Increase Inventory Count'
        print '4. Decrease Inventory Count'
        print '5. Finalize Changes'
        print '6. Exit Program'
        choice = raw_input("\nEnter your Choice: ")
        try:
            if int(choice) <= 6:
                return choice
            else:
                print 'Not a valid option. Please try again.'
        except:
            print 'Not a valid option. Please try again.'
        time.sleep(menuDelay)

# viewItemMenu() - print view item menu and accept barcode input
def viewItemMenu():
    menuHeader()
    while True:
        barcode = raw_input("Scan a barcode or press enter to return to previous menu\n")
        if barcode == '':
            break
        printInfoFromBarcode(barcode)

# addItemMenu() - creates new barcode for a new item and adds it to database in memory
def addItemMenu():
    menuHeader()

    lastBarcode = max(inv.iterkeys())  #find last barcode
    barcodeStart = lastBarcode[:4] #extract string portion from barcode
    newInvNumber = int(lastBarcode[4:]) + 1 #extract numerical value of barcode and increment it
    newBarcode = barcodeStart + str(newInvNumber).zfill(7) #combine the strin and int parts of the barcode with the correct formatting

    #Request item information from user
    print 'Enter the following information about the new inventory item.\n'
    shorthand = raw_input('Shorthand Name: ')
    name = raw_input('Item Name: ')
    count = raw_input('Initial Inventory Count: ')
    station = raw_input('Station: ')

    # create new item information dictionary and add it to the inventory dictionary
    details = {}
    details['row'] = newInvNumber + 1
    details['manufacturer'] = ''
    details['shorthand'] = shorthand
    details['name'] = name
    details['units'] = count
    details['station'] = station
    inv[newBarcode] = details

    #create new update object and add it to the list of updates
    newItem = Update('New', newBarcode, newInvNumber + 1, shorthand, name, count, station)
    updates.append(newItem)

# increaseItemMenu() - increases units of item in database in memory
def increaseItemMenu():
    menuHeader()

    while True:
        barcode = raw_input("Scan a barcode or press enter to return to previous menu\n") #wait for user to scan barcode
        if barcode == '':
            return
        printInfoFromBarcode(barcode) #print item information after barcode is scanned
        correctItem = raw_input('Is this the correct item? (y/n)') #query user if this is the correct item
        increment = 0

        if correctItem == 'y':
            while True:
                try:
                    increment = raw_input('\nEnter the number of items to add or press enter to go back: ') #ask for increment

                    if increment == '': #check if enter key is pressed
                        continue

                    increment = int(increment) #convert entered string to an int

                    break
                except:
                    print 'Please enter a valid number' #throw an exception if a number isnt entered
            inv[barcode]['units'] = str(float(inv[barcode]['units']) + increment) # update the units in the inventory dictionary
            print 'New inventory count for item: ' + inv[barcode]['units'] #print out current item count after change

            #create new update object and add it to the list of updates
            updatedItem = Update('Increase', barcode, inv[barcode]['row'], inv[barcode]['shorthand'], inv[barcode]['name'], inv[barcode]['units'], increment, inv[barcode]['station'])
            updates.append(updatedItem)

            time.sleep(menuDelay) # small delay for user to see the change
            break

        if correctItem == 'n':
            continue #repeat the loop if item scanned is not the same as the item displayed

        else:
            print "Invalid input entered"
            continue #repeat loop if invalid input enterd during confirmation

# decreaseItemMenu() - decreases units of item in database in memory
def decreaseItemMenu():
    menuHeader()

    while True:
        barcode = raw_input("Scan a barcode or press enter to return to previous menu\n") #wait for user to scan barcode
        if barcode == '':
            return
        printInfoFromBarcode(barcode) #print item information after barcode is scanned
        correctItem = raw_input('Is this the correct item? (y/n)') #query user if this is the correct item
        decrement = 0
        if correctItem == 'y':
            while True:
                try:
                    decrement = raw_input('\nEnter the number of items to remove or press enter to go back: ') #ask for decrement

                    if decrement == '': #check if enter key is pressed
                        continue

                    decrement = int(decrement) #convert entered string to an int

                    if decrement > float(inv[barcode]['units']): #check if decrement is larger than current inventory. loop if it is.
                        print "Number of items to decrement greater than current stock. Please try again."
                        continue

                    break
                except:
                    print 'Please enter a valid number' #throw an exception if a number isnt entered
            inv[barcode]['units'] = str(float(inv[barcode]['units']) - decrement) # update the units in the inventory dictionary
            print 'New inventory count for item: ' + inv[barcode]['units'] #print out current item count after change

            #create new update object and add it to the list of updates
            updatedItem = Update('Decrease', barcode, inv[barcode]['row'], inv[barcode]['shorthand'], inv[barcode]['name'], inv[barcode]['units'], decrement, inv[barcode]['station'])
            updates.append(updatedItem)

            time.sleep(menuDelay) # small delay for user to see the change
            break

        if correctItem == 'n':
            continue #repeat the loop if item scanned is not the same as the item displayed

        else:
            print "Invalid input entered"
            continue #repeat loop if invalid input enterd during confirmation

#finalizeChanges() - writes all updates to
def finalizeChanges():
    menuHeader()
    print 'Listed below are the changes that will be implemented:\n'

    #display all changes that will be implemented from the updates list
    for update in updates:
        print update

    choice = raw_input("\nWould you like to confirm these changes? (y/n) or type 'clear' to remove all changes.\n")

    if choice == 'y':

        # set font to be used while writing to local spreadsheet
        font = Font(name='Arial',
                    size=12,
                    bold=False,
                    italic=False,
                    vertAlign=None,
                    underline='none',
                    strike=False,)

        #run loop for each update in updates list
        for update in updates:
            if update.type == 'New':
                #add new item information to spreadsheet on a new line
                sheet[barColumn + str(update.row)] = update.barcode
                sheet[shorthandColumn + str(update.row)] = update.shorthand
                sheet[nameColumn + str(update.row)] = update.name
                try:
                    sheet[unitsColumn + str(update.row)] = int(update.units)
                except:
                    sheet[unitsColumn + str(update.row)] = float(update.units)
                sheet[stationColumn + str(update.row)] = update.station

                #Set correct font for data added to spreadsheet
                sheet[barColumn + str(update.row)].font = font
                sheet[shorthandColumn + str(update.row)].font = font
                sheet[nameColumn + str(update.row)].font = font
                sheet[unitsColumn + str(update.row)].font = font
                sheet[stationColumn + str(update.row)].font = font

            if update.type == 'Increase' or update.type == 'Decrease':
                #if item count needs to updated, only change the units for the item in the spreadsheet
                try:
                    sheet[unitsColumn + str(update.row)] = int(update.units)
                except:
                    sheet[unitsColumn + str(update.row)] = float(update.units)

                sheet[unitsColumn + str(update.row)].font = font #set correct font for data added

        wb.save('./inv.xlsx') # save the spreadsheet

    if choice == 'n':
        pass

    if choice == 'clear':
        del updates[:] #selete all updates if user enters clear

    else:
        print "Invalid input entered, returning to previous menu."


importDatabase() #imports the spreadsheet database into current memory by updating the inventory Database

#main while loop that displays main menu and switches screens based on user choices
while True:
    choice = mainMenu()

    if choice == '1':
        viewItemMenu()
    if choice == '2':
        addItemMenu()
    if choice == '3':
        increaseItemMenu()
    if choice == '4':
        decreaseItemMenu()
    if choice == '5':
        finalizeChanges()
    if choice == '6':
        quit()
