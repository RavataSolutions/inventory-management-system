# **README**
This repo contains Python source code for an inventory management system used by Ravata Solutions.
Command to clone project:  
"git clone https://RavataSolutions@bitbucket.org/RavataSolutions/inventory-management-system.git"

### ***Summary***
The inventory management system is a program that provides an interface to modify and update a local database from a spreadsheet. The program was designed to run on a Raspberry Pi, however can also be run on a local computer. It was developed in Python and uses the openpyxl framework to load and change spreadsheet data. A text based user menu helps select the operation that a user would need to perform which includes adding or updating inventory items. A barcode scanner can also be used with the program when prompts to enter barcode information are provided.

### ***Requirements***
Even though the application was designed for the Ravata Solutions Operations Raspberry Pi, it can run on any computer with terminal access. This includes terminal on macOS, Bash on Windows, or terminal on any Linux distribution. Some of the software you will need installed are:
+ Python 2.7+
+ pip (Python Package installer)
+ openpyxl

##### *Installing Python*
Use the command `sudo apt-get install python2`

##### *Installing pip*
Run the following commands
`curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py`
`python get-pip.py`

##### *Installing openpyxl*
Download openpyxl using the command `sudo -H pip install openpyxl`

### ***Operation***
At the start, the program goes through the current directory and searches for the database file. Once it has been found, the program loads all the database items into current memory (stored in a dictionary). Soon after, the user is provided with a main menu that allows selection of an operation on the database. The options that can be selected are:

+ View Item Information
+ Add New Inventory Item
+ Increase Inventory Count
+ Decrease Inventory Count
+ Finalize Changes
+ Exit

Once a selection is made, that particular function is then executed. More information on each of the functions is described below.

##### *View Item Information*
A prompt is presented to the user to scan a barcode. The user can either enter a barcode manually or use the barcode scanner to scan the barcode of an item. The function will then display the information stored in the database for the scanned item.

##### *Add New Inventory Item*
This function generates a new barcode based on the last barcode present in the current inventory. It then prompts the user to enter information about the item. This information is then saved in memory along with the newly generated barcode for the item. An update object is also created notifying that this new item needs to be added to the spreadsheet database once changes need to be finalized.

##### *Increase Inventory Count*
A prompt is presented to the user to scan a barcode. Once an item is scanned, the information of that item is displayed. The user is prompted again to specify whether the item displayed is the correct item or not. Once confirmed, the units of the item can be incremented and the items information in memory is updated to reflect that change. A new update object is also created at this point to notifying that this item needs to be updated once all changes are finalized.

##### *Decrease Inventory Count*
A prompt is presented to the user to scan a barcode. Once an item is scanned, the information of that item is displayed. The user is prompted again to specify whether the item displayed is the correct item or not. Once confirmed, the units of the item can be decremented and the items information in memory is updated to reflect that change. A new update object is also created at this point to notifying that this item needs to be updated once all changes are finalized.

##### *Finalize Changes*
This menu is accessed after all the relevant changes that a user wishes to make to the database are made. It presents the user with a list fo all the updates to the database that will be performed. The user has the option to confirm, reject, or clear all the changes. If confirmed, the program applies each of the updates to the spreadsheet database directly with the correct formatting.

##### *Exit*
Exits the program without making any further changes.

### ***Bug Log***
No bugs found yet.
